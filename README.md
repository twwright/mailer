# mailer
Mailer is a simple Docker-based Express + SMTP setup that can help speed up the development of HTML emails.

### Requirements
- Docker and Docker Compose

### Getting started
Run `docker-compose up -d` from the repo directory to start up Mailer, which consists of two containers: a simple Express server, and a SMTP server provided by the `namshi/smtp` Docker image.

Mailer by default exposes the Express server on port 8080 which is bound to port 8080 on the host -- play with `docker-compose.yml` to configure as desired.

Your email HTML files should go into the `views/` directory, this directory is mounted into Mailer for use by Express.

### Usage
Mailer exposes three `GET` endpoints for your email development:

- `/raw/:file`: Renders `:file` as-is
- `/render/:file`: Renders `:file` after passing it through a CSS inliner (provided by the `inline-css` package), which is the HTML as it will be sent in an email. The config used for `inline-css` is the `inlinerOptions` field of `config.json`
- `/mail/:file`: Passes `:file` through the CSS inliner and then mails the HTML (using the `nodemailer` package) using the SMTP server

### Mailer email settings
Mailer sends the email using `to`, `from`, and `subject` fields as defined by the `email` field of the configuration in `config.json`.

The `to`, `from`, and `subject` fields of the email can be overridden by corresponding query string parameters in the request to the `/mail/:file` endpoint.

e.g. `/mail/welcome.html?to=john@email.com` would address the email to `john@email.com` rather than the address defined in `config.json`.