'use strict';

const express = require('express');
const nodemailer = require('nodemailer');
const fs = require('fs');
const inlineCss = require('inline-css');

// read in email config
const config = require('./config.json');

// Constants
const PORT = 8080;

// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport('smtp://smtp');

// Express server
const app = express();

app.use('/raw', express.static('views'));

app.get('/', function(req, res) {
  res.redirect('/mail/default');
});

app.get('/render/:id', function(req, res) {

  console.log(req.url);

  // attempt to load html file
  var viewFilename = __dirname + '/views/' + req.params.id;
  fs.readFile(viewFilename, 'utf8', function(err, html) {
    if (err) {
      console.log('VIEW: ERROR:' + err);
      res.status(400).json({
        error: err
      });
    }

    // inline the css
    inlineCss(html, config.inlinerOptions).then(function(inlinedHtml) {

      console.log('VIEW:', viewFilename);

      // send to client
      res.send(inlinedHtml);
    });
  });
});

app.get('/mail/:id', function (req, res) {

  console.log(req.url);

  // attempt to load html file
  var viewFilename = __dirname + '/views/' + req.params.id;
  fs.readFile(viewFilename, 'utf8', function(err, html) {
    if (err) {
      console.log('ERROR:' + err);
      res.status(400).json({
        error: err
      });
    }

    // inline the css
    inlineCss(html, config.inlinerOptions).then(function(inlinedHtml) {
      // setup e-mail data
      var email = {
        from: config.email.from,
        to: config.email.to,
        subject: 'Mailer',
        text: 'Mailer',
        html: inlinedHtml
      };

      if(req.query.from)
        email.from = req.query.from;
      if(req.query.to)
        email.to = req.query.to;
      if(req.query.subject)
        email.subject = req.query.subject;

      // send mail with defined transport object
      transporter.sendMail(email, function(error, info){
        if(error) {
          console.log("SMTP: ERROR: " + error);
          return res.status(500).json({
            error: error
          });
        }
        console.log('SMTP: ' + info.response);
        res.json({
          view: viewFilename,
          email: email,
          response: info.response
        });
      });
    });
  });
});

app.listen(PORT);
console.log('Running on http://localhost:' + PORT);
